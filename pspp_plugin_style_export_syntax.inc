<?php
/**
 * @file
 *   Plugin include file for export style plugin.
 */

/**
 * Generalized style plugin for export plugins.
 *
 * @ingroup views_style_plugins
 */
class pspp_plugin_style_export_syntax extends pspp_plugin_style_export {
  var $feed_text = 'PSPP Syntax';
  var $feed_file = 'view-%view.sps';

  /**
   * Initialize plugin.
   *
   * Set feed image for shared rendering later.
   */
  function init(&$view, &$display, $options = NULL) {
    parent::init($view, $display, $options = NULL);
    $this->feed_image = drupal_get_path('module', 'pspp') . '/images/pspp-syntax.png';

  }
  
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    unset($form['parent_sort']);
  }
  
  function validate() {
    $errors = parent::validate();

    $linked_id = FALSE;
    if (isset($this->display->display_options['link_display'])) {
      $linked_id = $this->display->display_options['link_display'];
    }
    elseif (isset($this->view->display['default']->display_options['link_display'])) {
      $linked_id = $this->view->display['default']->display_options['link_display'];
    }

    $linked = & $this->view->display[$linked_id];
    if ($linked->display_options['style_plugin'] != 'views_pspp_csv') {
      $errors[] = t('Display "%display" is a PSPP Syntax style and must be linked to a PSPP CSV display style. Use the "Link display" setting to link to the proper PSPP CSV display.', array('%display' => $this->display->display_title));
    }
    else {
      if (empty($linked->display_options['style_options']['filename'])) {
        $errors[] = t('Display %display is linked to a PSPP CSV display %linked that has no filename defined. PSPP Syntax need the filename of the data file to be referred. Enter %linked display, edit the style settings and enter a filename under "Provide as file" option.', array('%display' => $this->display->display_title, '%linked' => $linked->display_title));
      }
    }

    return $errors;
  }
}
