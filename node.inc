<?php

function node_pspp_map(& $pspp_fields) {
  $node_fields = & node_pspp_definition();

  foreach ($pspp_fields as $field_key => $field) {
    if (isset($node_fields[$field['id']])) {
      $pspp_fields[$field_key]['module'] = 'node';
      $pspp_fields[$field_key]['type'] = is_array($node_fields[$field['id']]) ? $node_fields[$field['id']]['type'] : $node_fields[$field['id']];
    }
  }
}

function node_pspp_definition() {
  static $node_fields;
  $pspp_date_format = variable_get('pspp_date_format', 'EDATE10');
  $pspp_generic_date = array(
    'type' => $pspp_date_format,
    'callbacks' => array(
      'format_date' => array('custom', pspp_date_format($pspp_date_format)),
    )
  );
  $pspp_body = array(
    'type' => PSPP_TYPE_A,
    'callbacks' => array(
      'transliteration_get' => array('?'),
      'drupal_substr' => array(0, 32767),
    )
  );
  if (!isset($node_fields)) {
    $node_fields = array(
      'nid' => PSPP_TYPE_F10,
      'vid' => PSPP_TYPE_F10,
      'type' => array(
        'type' => PSPP_TYPE_A32,
        'callbacks' => array(
          'transliteration_get' => array('?'),
        )
      ),
      'language' => PSPP_TYPE_A12,
      'title' => array(
        'type' => PSPP_TYPE_A255,
        'callbacks' => array(
          'transliteration_get' => array('?'),
        )
      ),
      'uid' => PSPP_TYPE_F11,
      'status' => PSPP_TYPE_F11,
      'created' => $pspp_generic_date,
      'changed' => $pspp_generic_date,
      'comment' => PSPP_TYPE_F11,
      'promote' => PSPP_TYPE_F11,
      'moderate' => PSPP_TYPE_F11,
      'sticky' => PSPP_TYPE_F11,
      'tnid' => PSPP_TYPE_F10,
      'translate' => PSPP_TYPE_F11,
      'revision_uid' => PSPP_TYPE_F11,
      'body' => $pspp_body,
      'teaser' => $pspp_body,
      'log' => $pspp_body,
      'format' => PSPP_TYPE_F11,
      'name' => array(
        'type' => PSPP_TYPE_A60,
        'callbacks' => array(
          'transliteration_get' => array('?'),
        )
      ),
      'picture' => PSPP_TYPE_A255,
      'path' => PSPP_TYPE_A128,
      'last_comment_timestamp' => $pspp_generic_date,
      'last_comment_name' => PSPP_TYPE_A60,
      'comment_count' => PSPP_TYPE_F10,
    );
  }
  return $node_fields;
}

function node_pspp_value($field_value, $field_key, & $pspp) {
  $node_fields = & node_pspp_definition();
  $field_name = $pspp[$field_key]['id'];
  
  // It's a simple mapping, return the value as it is.
  if (!isset($node_fields[$field_name]['callbacks']) || !is_array($node_fields[$field_name]['callbacks'])) {
    return $field_value;
  }
  
  foreach ($node_fields[$field_name]['callbacks'] as $callback => $arguments) {
    if (is_array($arguments)) {
      array_unshift($arguments, $field_value);
    }
    else {
      $arguments = array($field_value);
    }
    if (function_exists($callback)) {
      $field_value = call_user_func_array($callback, $arguments);
    }
  }
  return $field_value;
}
