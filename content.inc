<?php

function content_pspp_map(& $pspp_fields) {
  $cck_fields = & content_pspp_definition();

  foreach ($pspp_fields as $field_key => $field) {
    if (in_array($field['id'], $cck_fields)) {
      $pspp_fields[$field_key]['module'] = 'content';
      $pspp_fields[$field_key]['type'] = content_pspp_get_type($field);
      
      // Link also CCK informations.
      $pspp_fields[$field_key]['options']['content'] = & $field['views_field']->content_field;
    }
  }
}

function content_pspp_definition() {
  static $cck;
  if (!isset($cck)) {
    $cck_info = & _content_type_info();
    //p($cck_info['field types']);
    foreach ($cck_info['fields'] as $field_id => $field) {
      if (in_array($field['module'], array('nodereference', 'userreference', 'number', 'text'))) {
        $cck[] = $field_id;
      }
    }
  }
  return $cck;
}

function content_pspp_get_type($field) {
  switch ($field['views_field']->content_field['type']) {
    case 'userreference':
    case 'nodereference':
    case 'number_integer':
      return PSPP_TYPE_F11;
    break;
    case 'number_decimal':
      return 'F'. $field['views_field']->content_field['precision'] .'.'. $field['views_field']->content_field['scale'];
    break;
    case 'number_float':
      return 'E40.15';
    break;
    case 'text':
      if (is_numeric($field['views_field']->content_field['max_length']) && $field['views_field']->content_field['max_length'] > 0) {
        return PSPP_TYPE_A . $field['views_field']->content_field['max_length'];
      }
      return PSPP_TYPE_A;
    break;
  }
}
/*
function content_pspp_value($field_value, $field_key, & $pspp) {
  switch ($pspp[$field_key]['options']['content']['type']) {
    //case 'number_float':
    //case 'number_decimal':
      
  }
}
*/