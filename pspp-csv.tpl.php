<?php
/**
 * @file pspp-csv.tpl.php
 * Template to display a view in CSV format.
 *
 * - $pspp: an associative array with fields as keys and PSPP types as values.
 * - $items: An array with "PSPP Ready" values.
 * - $separator: The separator used to seperate fields. generally comma's but
 *   sometimes people use other values in CSVs.
 * @ingroup views_templates
 */

// Print out header row, if option was selected.
if ($options['header']) {
  $fields = array();
  foreach ($pspp as $field_key => $field) {
    $fields[] = $field['id'];
  }
  print implode($separator, $fields) . "\r\n";
}

// Print out exported items.
foreach ($items as $item):
  print implode($separator, $item) . "\r\n";
endforeach;
