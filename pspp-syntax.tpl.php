<?php
/**
 * @file pspp-syntax.tpl.php
 * Template to display a view as PSPP Syntax.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $rows: An array of row items. Each row is an array of content
 *   keyed by field ID.
 * - $pspp: an associative array with fields as keys and PSPP types as values.
 * - $themed_rows: a array of rows with themed fields.
 * - $items:
 * - $separator: The separator used to seperate fields. generally comma's but
 *   sometimes people use other values in CSVs.
 * @ingroup views_templates
 */

$fields = array();
foreach ($pspp as $field_key => $field) {
  $fields[] = "    {$field["id"]} {$field["type"]}";
}
$fields = implode("\n", $fields);
print <<<EOS
GET DATA
  /TYPE=TXT
  /FILE="{$pspp_file}"
  /DELCASE=LINE
  /DELIMITERS=","
  /ARRANGEMENT=DELIMITED
  /FIRSTCASE=2
  /IMPORTCASE=ALL
  /VARIABLES=
{$fields}.
CACHE.
EXECUTE.
EOS;

// Print out header row, if option was selected.
//if ($options['header']) {
//  print implode($separator, $header) . "\r\n";
//}

// Print out exported items.
//foreach ($themed_rows as $count => $item_row):
//  print implode($separator, $item_row) . "\r\n";
//endforeach;
