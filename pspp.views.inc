<?php
/**
 * @file
 * Views include file with views hooks.
 */

/**
 * Implementation of hook_views_plugins().
 */
function pspp_views_plugins() {
  $path = drupal_get_path('module', 'pspp');
  return array(
    'style' => array(
      'pspp' => array(
        // this isn't really a display but is necessary so the file can
        // be included.
        'no ui' => TRUE,
        'handler' => 'pspp_plugin_style_export',
        'path' => $path,
      ),
      'views_pspp_csv' => array(
        'title' => t('PSPP CSV file'),
        'help' => t('Display the view as a comma seperated list.'),
        'path' => $path,
        'handler' => 'pspp_plugin_style_export_csv',
        'parent' => 'pspp',
        'theme' => 'pspp_csv',
        'theme file' => 'pspp.theme.inc',
        'uses row plugin' => FALSE,
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'type' => 'feed',
        // PSPP element that will be used to set additional headers when serving the feed.
        'export headers' => array('Content-type: text/plain; charset=utf-8'),
        // PSPP element mostly used for creating some additional classes and template names.
        'export feed type' => 'csv',
      ),
      'views_pspp_syntax' => array(
        'title' => t('PSPP Syntax file'),
        'help' => t('Display the view as a PSPP Syntax file.'),
        'path' => $path,
        'handler' => 'pspp_plugin_style_export_syntax',
        'parent' => 'pspp',
        'theme' => 'pspp_syntax',
        'theme file' => 'pspp.theme.inc',
        'uses row plugin' => FALSE,
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'type' => 'feed',
        'export headers' => array('Content-Type: text/plain; charset=utf-8'),
        'export feed type' => 'syntax',
      ),
    ),
  );
}
