
Requirements
============

Views and Transliteration modules are required.

Install
=======

Follow basic Drupal installtion procedures.

Enabling
========

Enable the module from Drupal UI.

Usage
=====

- Edit or add a view
- Add a new Feed Display
- Change its style to PSPP CSV
- Add a new Feed Display and change change its style to PSPP Syntax
- Link the PSPP Syntax display to PSPP CSV so that the syntax display knows that is linked to that data display.
- You can configure if you want that those displays are printed in the browser or downloaded
- You can attach the two displays to other display, like a Page display.

Developers
==========

Additional types can be integrated via hooks. Documentation to come!

Maintainers
===========
- Claudiu Cristea - http://drupal.org/user/56348
- Gabriel Dragomir - http://drupal.org/user/154006

